import os
from utils import is_allowed_message, is_valid_size
from fastapi import FastAPI, Request
import aiohttp
from slowapi import Limiter
from fastapi.middleware.cors import CORSMiddleware

O3CH_WEBHOOK = os.environ["O3CH_WEBHOOK"]

app = FastAPI()

origins = [
    "http://localhost:8000",
    "http://127.0.0.1:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_ipaddr(request: Request) -> str:
    if "x-forwarded-for" in request.headers:
        return request.headers["x-forwarded-for"]
    else:
        if not request.client or not request.client.host:
            return "127.0.0.1"
        return request.client.host


limiter = Limiter(key_func=get_ipaddr)


async def send_to_chat(msg: str):
    data = {
        "text": msg
    }

    async with aiohttp.ClientSession() as session:
        async with session.post(O3CH_WEBHOOK, json=data) as response:
            response.raise_for_status()


@app.get("/debug/ip")
async def show_ip(request: Request):
    """Ручка нужна для проверки, что через cloudflare приходит нормальный ип.
    В свою очередь, ип нужен только для рейт лимита"""
    return {"ip": get_ipaddr(request)}


@app.post("/")
@limiter.limit("5/minute")
async def index(request: Request):
    """Ручка для отправки сообщения"""
    message = await request.body()
    message = message.decode()

    if not is_valid_size(message):
        return {"message": "Нельзя больше 900 символов или пустые строки", "error": True}

    if not is_allowed_message(message):
        return {"message": "Сообщение содержит запрещенные символы", "error": True}

    await send_to_chat(message)
    return {"message": "ok", "error": False}
